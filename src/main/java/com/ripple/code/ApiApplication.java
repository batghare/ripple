package com.ripple.code;


import com.google.inject.Guice;
import com.google.inject.Injector;
import com.ripple.code.dao.TransferDAO;
import com.ripple.code.resources.PaymentResource;
import com.ripple.code.resources.RippleDepositResource;
import com.ripple.code.resources.RipplePaymentResource;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class ApiApplication extends Application<ApiConfiguration> {

    public static void main(final String[] args) throws Exception {
        new ApiApplication().run(args);
    }

    @Override
    public String getName() {
        return "Joyride APIs";
    }

    @Override
    public void initialize(final Bootstrap<ApiConfiguration> bootstrap) {

        // Enable variable substitution with environment variables
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );

    }

    @Override
    public void run(final ApiConfiguration configuration,
                    final Environment environment) {

        ApiModule apiModule = new ApiModule();
        Injector injector = Guice.createInjector(apiModule);

        final PaymentResource paymentResource = new RipplePaymentResource(configuration, injector.getInstance(TransferDAO.class));
        final RippleDepositResource rippleDepositResource = new RippleDepositResource(configuration, injector.getInstance(TransferDAO.class));

        environment.jersey().register(paymentResource);
        environment.jersey().register(rippleDepositResource);

        // Enable CORS headers
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "Authorization, X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
    }
}

