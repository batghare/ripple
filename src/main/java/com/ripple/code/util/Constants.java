package com.ripple.code.util;

public class Constants {
    public static final String HISTORY = "/history";
    public static final String BALANCE = "/balance";
    public static final String PAYMENT = "/payment";
    public static final String DEPOSIT = "/deposit";
}
