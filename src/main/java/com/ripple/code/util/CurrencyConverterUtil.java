package com.ripple.code.util;

import com.ripple.code.model.Currency;


//Currency converter, restricted to only 3 currencies and returns value in USD.
// It can be replaced by some global currency converter.
public class CurrencyConverterUtil {

    public static double convertCurrencyToUSD(double amount, Currency currency) {
        switch (currency) {
            case INR:
                return amount / 70;
            case GBP:
                return (amount * 1.30);
        }
        return amount;
    }

}
