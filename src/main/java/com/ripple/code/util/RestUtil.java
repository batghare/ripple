package com.ripple.code.util;

import com.ripple.code.model.PaymentTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class RestUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestUtil.class);

    private static Client client;
    private static WebTarget target;

    public static String makePostRequest(String inputUrl, PaymentTransfer transfer) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("making post request to :" + inputUrl);
        }
        client = ClientBuilder.newClient();
        target = client.target(inputUrl);
        Response response = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(transfer, MediaType.APPLICATION_JSON));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("received post from  :" + inputUrl + response.getStatus());
        }

        if (response.getStatus() != Response.Status.CREATED.getStatusCode()) {
            throw new RuntimeException("Failed to send transfer to partner");
        }

        return response.readEntity(String.class);
    }
}






