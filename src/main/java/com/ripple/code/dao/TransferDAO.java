package com.ripple.code.dao;

import com.ripple.code.model.PaymentTransfer;

import javax.validation.Valid;
import java.util.List;

public interface TransferDAO {
    void transactionEntry(@Valid PaymentTransfer transfer);

    double getBalance();

    List<PaymentTransfer> getHistory();
}
