package com.ripple.code.dao;

import com.ripple.code.model.PaymentTransfer;
import com.ripple.code.util.CurrencyConverterUtil;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

public class RippleTransferDAO implements TransferDAO {

    private List<PaymentTransfer> transferList = new ArrayList<>();

    @Override
    public void transactionEntry(@Valid PaymentTransfer transfer) {

        double money = transfer.getMoney();

        money = CurrencyConverterUtil.convertCurrencyToUSD(money, transfer.getCurrency());

        if (transferList.size() == 0) {
            transfer.setCurrentBalance(money);
        } else {
            transfer.setCurrentBalance(transferList.get(transferList.size() - 1).getCurrentBalance() + money);
        }

        transferList.add(transfer);
    }

    @Override
    public double getBalance() {
        if (transferList.size() == 0) {
            return 0;
        }
        return transferList.get(transferList.size() - 1).getCurrentBalance();
    }

    @Override
    public List<PaymentTransfer> getHistory() {
        return transferList;
    }
}
