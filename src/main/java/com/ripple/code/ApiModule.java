package com.ripple.code;

import com.google.inject.AbstractModule;
import com.ripple.code.dao.RippleTransferDAO;
import com.ripple.code.dao.TransferDAO;

public class ApiModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(TransferDAO.class).to(RippleTransferDAO.class).asEagerSingleton();
    }
}
