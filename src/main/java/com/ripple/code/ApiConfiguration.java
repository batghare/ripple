package com.ripple.code;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

import javax.validation.constraints.NotEmpty;


public class ApiConfiguration extends Configuration {

    @NotEmpty
    private String partyName;

    @NotEmpty
    private String partyServerUrl;

    @JsonProperty("partyName")
    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    @JsonProperty("partyServerUrl")
    public String getPartyServerUrl() {
        return partyServerUrl;
    }

    public void setPartyServerUrl(String partyServerUrl) {
        this.partyServerUrl = partyServerUrl;
    }
}
