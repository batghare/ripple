package com.ripple.code.resources;

import com.ripple.code.model.Payment;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import static com.ripple.code.util.Constants.BALANCE;
import static com.ripple.code.util.Constants.HISTORY;

public interface PaymentResource {
    @POST
    Response createTransfer(@Valid Payment payment);

    @GET
    @Path(BALANCE)
    Response getBalance();

    @GET
    @Path(HISTORY)
    Response getHistory();
}
