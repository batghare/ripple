package com.ripple.code.resources;

import com.ripple.code.model.PaymentTransfer;

import javax.ws.rs.POST;
import javax.ws.rs.core.Response;

public interface DepositResource {
    @POST
    Response createDeposit(PaymentTransfer transfer);
}
