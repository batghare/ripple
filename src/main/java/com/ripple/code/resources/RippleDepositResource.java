package com.ripple.code.resources;

import com.google.inject.Inject;
import com.ripple.code.ApiConfiguration;
import com.ripple.code.dao.TransferDAO;
import com.ripple.code.model.PaymentTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.UUID;

import static com.ripple.code.util.Constants.DEPOSIT;

@Path(DEPOSIT)
@Produces(MediaType.APPLICATION_JSON)
//Resource to accept payment from other party.
public class RippleDepositResource implements DepositResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(RippleDepositResource.class);

    private final ApiConfiguration configuration;
    private final TransferDAO TransferDAO;

    @Inject
    public RippleDepositResource(ApiConfiguration configuration, TransferDAO TransferDAO) {
        this.TransferDAO = TransferDAO;
        this.configuration = configuration;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("RipplePaymentResource is initialized");
            LOGGER.debug("Application configuration partnerServerUrl" + configuration.getPartyServerUrl());
        }
    }

    @Override
    @POST
    public Response createDeposit(PaymentTransfer transfer) {
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("RippleDepositResource.createDeposit is requested");
            }

            String trackingId = UUID.randomUUID().toString();
            transfer.setDestTrackingId(trackingId);
            transfer.setPartyInvolved(configuration.getPartyName());

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("RippleDepositResource.createDeposit starting to update transaction history and current balanace");
            }

            TransferDAO.transactionEntry(transfer);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("RippleDepositResource.createDeposit end update transaction history and current balanace");
            }
            return Response.status(Response.Status.CREATED).entity(trackingId).build();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("RippleDepositResource.createDeposit failed:" + e.getMessage() + Arrays.toString(e.getStackTrace()));
            }
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();

        }
    }
}
