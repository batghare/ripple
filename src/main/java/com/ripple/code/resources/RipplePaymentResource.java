package com.ripple.code.resources;

import com.google.inject.Inject;
import com.ripple.code.ApiConfiguration;
import com.ripple.code.dao.TransferDAO;
import com.ripple.code.model.Payment;
import com.ripple.code.model.PaymentTransfer;
import com.ripple.code.util.RestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.ripple.code.util.Constants.*;

@Path(PAYMENT)
@Produces(MediaType.APPLICATION_JSON)

// Resource to make payment from one party to other. It also exposes apis to get payment history (credit or debit) and
// current balance of owner party.
public class RipplePaymentResource implements PaymentResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(RipplePaymentResource.class);

    private final TransferDAO TransferDAO;

    private final ApiConfiguration configuration;

    @Inject
    public RipplePaymentResource(ApiConfiguration configuration, TransferDAO TransferDAO) {
        this.TransferDAO = TransferDAO;
        this.configuration = configuration;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("RipplePaymentResource is initialized");
            LOGGER.debug("Application configuration partnerServerUrl" + configuration.getPartyServerUrl());
        }
    }

    @Override
    @POST
    public Response createTransfer(@Valid Payment payment) {
        try {

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("RipplePaymentResource.createTransfer is requested");
            }

            String sourceTrackingId = UUID.randomUUID().toString();
            //create new Payment transfer entry;

            Date date = new Date();

            PaymentTransfer transfer = new PaymentTransfer(payment.getMoney(), date,
                    sourceTrackingId, "", null, payment.getCurrency());

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("RipplePaymentResource.createTransfer starting call to deposit money to partner");
            }

            //Call Deposit API of partner
            String destTrackingId = RestUtil.makePostRequest(configuration.getPartyServerUrl() + "deposit", transfer);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("RipplePaymentResource.createTransfer end call to deposit money to partner, " +
                        "received dest tracking id :" + destTrackingId);
            }

            transfer = new PaymentTransfer(-payment.getMoney(), date,
                    sourceTrackingId, destTrackingId, configuration.getPartyName(), payment.getCurrency());

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("RipplePaymentResource.createTransfer starting to update transaction history and current balance");
            }

            TransferDAO.transactionEntry(transfer);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("RipplePaymentResource.createTransfer end update transaction history and current balance");
            }

            return Response.status(Response.Status.CREATED).entity(sourceTrackingId).build();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("RipplePaymentResource.createTransfer failed:" + e.getMessage() + Arrays.toString(e.getStackTrace()));
            }
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @Override
    @GET
    @Path(BALANCE)
    public Response getBalance() {
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("RipplePaymentResource.getBalance is requested");
            }
            double balance = TransferDAO.getBalance();
            return Response.status(Response.Status.OK).entity(balance).build();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("RipplePaymentResource.getBalance failed:" + e.getMessage() + Arrays.toString(e.getStackTrace()));
            }
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @Override
    @GET
    @Path(HISTORY)
    public Response getHistory() {
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("RipplePaymentResource.getHistory is requested");
            }
            List<PaymentTransfer> list = TransferDAO.getHistory();
            return Response.status(Response.Status.OK).entity(list).build();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("RipplePaymentResource.getHistory failed:" + e.getMessage() + Arrays.toString(e.getStackTrace()));
            }
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }
}
