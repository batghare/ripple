package com.ripple.code.model;

//List of Currency, restricted to 3 for demo purpose.
public enum Currency {
    USD,
    INR,
    GBP
}
