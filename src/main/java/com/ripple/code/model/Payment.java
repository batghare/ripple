package com.ripple.code.model;

import javax.validation.constraints.NotNull;

//DTO to request to make payment from one party to other.
public class Payment {

    @NotNull
    private double money;

    @NotNull
    private Currency currency;

    public Payment() {

    }

    public Payment(double money, Currency currency) {
        this.money = money;
        this.currency = currency;
    }

    public double getMoney() {
        return money;
    }

    public Currency getCurrency() {
        return currency;
    }
}
