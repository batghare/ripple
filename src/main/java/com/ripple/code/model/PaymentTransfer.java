package com.ripple.code.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

//DTO to transfer data between servers/parties
public class PaymentTransfer {

    @NotNull
    private double money;
    private double currentBalance;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MMMMM yyyy HH:mm:ss.SSSZ")
    private Date date;
    private String sourceTrackingId;
    private String destTrackingId;
    private String partyInvolved;
    @NotNull
    private Currency currency;

    public PaymentTransfer() {
    }

    public PaymentTransfer(double money, Date date, String sourceTrackingId, String destTrackingId, String partyInvolved, Currency currency) {
        this.money = money;
        this.date = date;
        this.sourceTrackingId = sourceTrackingId;
        this.destTrackingId = destTrackingId;
        this.partyInvolved = partyInvolved;
        this.currency = currency;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSourceTrackingId() {
        return sourceTrackingId;
    }

    public void setSourceTrackingId(String sourceTrackingId) {
        this.sourceTrackingId = sourceTrackingId;
    }

    public String getDestTrackingId() {
        return destTrackingId;
    }

    public void setDestTrackingId(String destTrackingId) {
        this.destTrackingId = destTrackingId;
    }

    public String getPartyInvolved() {
        return partyInvolved;
    }

    public void setPartyInvolved(String partyInvolved) {
        this.partyInvolved = partyInvolved;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
