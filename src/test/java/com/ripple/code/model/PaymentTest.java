package com.ripple.code.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

public class PaymentTest {

    @Test
    public void paymentTest() throws IOException {
        Payment payment = new Payment(14.00, Currency.USD);
        assert (payment.getMoney() == 14.00);
        assert (payment.getCurrency() == Currency.USD);
        ObjectMapper objectMapper = new ObjectMapper();
        String string = objectMapper.writeValueAsString(payment);
        payment = objectMapper.readValue(string, Payment.class);
        assert (payment.getMoney() == 14.00);
    }
}
