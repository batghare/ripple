package com.ripple.code.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;

public class PaymentTransferTest {

    @Test
    public void paymentTransferTest() throws IOException {

        PaymentTransfer paymentTransfer = new PaymentTransfer(14.00, new Date(),
                "test", "test", "test", Currency.USD);

        ObjectMapper objectMapper = new ObjectMapper();

        String string = objectMapper.writeValueAsString(paymentTransfer);
        paymentTransfer = objectMapper.readValue(string, PaymentTransfer.class);
        assert (paymentTransfer.getMoney() == 14.00);
        assert (paymentTransfer.getCurrency() == Currency.USD);
        assert (paymentTransfer.getCurrentBalance() == 0.00);
        assert (paymentTransfer.getDestTrackingId().equals("test"));
        assert (paymentTransfer.getSourceTrackingId().equals("test"));
        assert (paymentTransfer.getPartyInvolved().equals("test"));
        assert (paymentTransfer.getDate() != null);
        paymentTransfer.setCurrentBalance(14.00);
        assert (paymentTransfer.getCurrentBalance() == 14.00);

    }

}
