package com.ripple.code.resources;

import com.ripple.code.ApiApplication;
import com.ripple.code.ApiConfiguration;
import com.ripple.code.model.Currency;
import com.ripple.code.model.Payment;
import com.ripple.code.model.PaymentTransfer;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;

import static com.ripple.code.util.Constants.*;

public class IntegrationTest {
    @ClassRule
    public static final DropwizardAppRule<ApiConfiguration> ALICE_RULE =
            new DropwizardAppRule<>(ApiApplication.class, ResourceHelpers.resourceFilePath("ApiConfiguration.Alice.yml"));
    @ClassRule
    public static final DropwizardAppRule<ApiConfiguration> BOB_RULE =
            new DropwizardAppRule<>(ApiApplication.class, ResourceHelpers.resourceFilePath("ApiConfiguration.Bob.yml"));
    private static final String HTTP_LOCALHOST = "http://localhost:";
    private static Client client;

    @BeforeClass
    public static void setUp() {
        client = ClientBuilder.newClient();
    }

    @Test
    public void paymentBetweenPartiesTest() {
        final Payment payment = new Payment(14.00, Currency.USD);

        String trackingId = client.target(HTTP_LOCALHOST + ALICE_RULE.getLocalPort() + PAYMENT)
                .request()
                .post(Entity.entity(payment, MediaType.APPLICATION_JSON_TYPE))
                .readEntity(String.class);

        assert (trackingId != null);

        double balance = client.target(HTTP_LOCALHOST + BOB_RULE.getLocalPort() + PAYMENT + BALANCE)
                .request()
                .get()
                .readEntity(double.class);
        assert (balance == 14.00);

        balance = client.target(HTTP_LOCALHOST + ALICE_RULE.getLocalPort() + PAYMENT + BALANCE)
                .request()
                .get()
                .readEntity(double.class);
        assert (balance == -14.00);

        trackingId = client.target(HTTP_LOCALHOST + ALICE_RULE.getLocalPort() + PAYMENT)
                .request()
                .post(Entity.entity(payment, MediaType.APPLICATION_JSON_TYPE))
                .readEntity(String.class);

        assert (trackingId != null);

        balance = client.target(HTTP_LOCALHOST + BOB_RULE.getLocalPort() + PAYMENT + BALANCE)
                .request()
                .get()
                .readEntity(double.class);
        assert (balance == 28.00);

        balance = client.target(HTTP_LOCALHOST + ALICE_RULE.getLocalPort() + PAYMENT + BALANCE)
                .request()
                .get()
                .readEntity(double.class);
        assert (balance == -28.00);

        trackingId = client.target(HTTP_LOCALHOST + BOB_RULE.getLocalPort() + PAYMENT)
                .request()
                .post(Entity.entity(payment, MediaType.APPLICATION_JSON_TYPE))
                .readEntity(String.class);

        assert (trackingId != null);

        balance = client.target(HTTP_LOCALHOST + ALICE_RULE.getLocalPort() + PAYMENT + BALANCE)
                .request()
                .get()
                .readEntity(double.class);
        assert (balance == -14.00);

        balance = client.target(HTTP_LOCALHOST + BOB_RULE.getLocalPort() + PAYMENT + BALANCE)
                .request()
                .get()
                .readEntity(double.class);
        assert (balance == 14.00);

        List<PaymentTransfer> transferList = client.target(HTTP_LOCALHOST + ALICE_RULE.getLocalPort() + PAYMENT + HISTORY)
                .request()
                .get()
                .readEntity(new GenericType<List<PaymentTransfer>>() {
                });
        assert (transferList.size() == 3);

        assert (transferList.get(2).getCurrentBalance() == -14.00);

        transferList = client.target(HTTP_LOCALHOST + BOB_RULE.getLocalPort() + PAYMENT + HISTORY)
                .request()
                .get()
                .readEntity(new GenericType<List<PaymentTransfer>>() {
                });
        assert (transferList.size() == 3);

        assert (transferList.get(2).getCurrentBalance() == 14.00);
    }
}