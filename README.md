# Ripple Code challenge
## Problem definition
    A “trustline” is a way to keep track of debt between two parties. 
    The trustline balance starts at 0 and both parties independently track their views of the balance. 
    For example, if Alice owes Bob €10, then Alice sees a balance of -10 and Bob sees a balance of 10. 
    If Alice increases her debt to Bob by €10 more, then her balance would be -20, and Bob’s balance would be 20.

## Pre Req.
    Maven and jdk 1.8 is required to build this project.
## Configuration.

    There are 2 Api Configuration files under test directory. 
    These settings can be changed in configuration files for different ports or party names
  
    1. Alice - party1 - runs on http port - 8080 and expects other party to run on port 9090
    2. Bob   - party2 - runs on http port - 9090 and expects other party to run on port 8080
  
    
  
 ## Build

      Build the project as
      mvn clean package -DskipTests (Running Test will take more time) It generated fat/uber jar with all dependencies.
 ## Start Servers
      Get to target directory of project.
      1.0 To start server for party1 - > Alice 
        java -jar CodeChallenge-1.0.jar server test-classes/ApiConfiguration.Alice.yml
      
      2.0 To start server for party2 - > Bob 
        java -jar CodeChallenge-1.0.jar server test-classes/ApiConfiguration.Bob.yml
     

## Execute APIs
    Please Check Integration tests for how to execute APIs.
        To check balanace on each server  - GET API
        http://localhost:{port}/payment/balance 
        
        To check history on each server - GET API
        http://localhost:{port}/payment/history
        
        To make payment to party - POST API  
        http://localhost:9090/payment
        with payload (3 currencies supported right now)
        {"money":20.0,"currency":"GBP"} or
        {"money":20.0,"currency":"INR"} or
        {"money":20.0,"currency":"USD"}


